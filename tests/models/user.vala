using Valse;

/*
* Represents an user.
*
* Used in tests.
*/
public class User : Object {
    /*
    * The username of this user.
    */
    public string pseudo { get; set; }

    /*
    * The signature of this user.
    */
    public string sign { get; set; }

    /*
    * The URL of the avatar to use for this user.
    */
    public string avatar { get; set; }

    /*
    * The website of the user.
    */
    public string website { get; set; }

    /*
    * The date on which the user created his account.
    *
    * Used to test dates.
    */
    public DateTime register_date { get; set; }

    /*
    * The post of this user.
    *
    * Used to test Many<G>.
    */
    public Many<string> posts { get; set; default = new Many<string> (); }
}
