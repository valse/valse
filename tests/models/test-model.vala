/*
* A model to make tests.
*/
public class TestModel : Object {

    public TestModel (string b, string u) {
        this.blob = b;
        this.url = u;
    }

    public string blob { get; set; default = "plop"; }

    public string url { get; set; default = "/zds"; }
}
