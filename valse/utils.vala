using Gee;

namespace Valse {
    const string NAME = "Valse";
    const string VERSION = "-dev";

    public DebugLevel log_level = DebugLevel.DEBUG;
    const string DEBUG_DATE_FORMAT = "%d/%b/%Y %T";
    const int EXIT_SUCCESS = 1;
    const int EXIT_ON_ERROR = -1;

    /**
    * Splits a path.
    *
    * @param path The path to split
    * @return An array with the different parts of the path
    */
    private string[] split_path (string path) {
        string[] result = new string[] {};
        foreach (string part in path.split ("/")) {
            if (part.length != 0) {
                // we add to the result, only if it's not empty
                result += part;
            }
        }

        return result;
    }

    internal void log_debug (string message) {
        if (log_level == DebugLevel.DEBUG) {
            print (@"[DEBUG] $(message)\n");
        }
    }

    internal void log_warning (string warn) {
        if (log_level >= DebugLevel.WARNINGS) {
            print (@"[WARNING] $(warn)\n");
        }
    }

    internal void log_error (string error) {
        if (log_level >= DebugLevel.ERRORS) {
            print (@"[ERROR] $(error)\n");
        }
    }

    internal void log (string message) {
        if (log_level >= DebugLevel.BASIC) {
            print (@"$(message)\n");
        }
    }

    /**
    * Decode an URI
    *
    * @param encoded The encoded URI to decode
    */
    public string decode_uri (string encoded, bool for_form = true) {
        HashMap<string, string> decoder = new HashMap<string, string> ();
        if (for_form) {
            decoder[" "] = "+";
        } else {
            decoder[" "] = "%20";
        }
        decoder["\r"] = "%0A";
        decoder["\n"] = "%0D";
        decoder["!"] = "%21";
        decoder["\""] = "%22";
        decoder["#"] = "%23";
        decoder["$"] = "%24";
        decoder["%"] = "%25";
        decoder["&"] = "%26";
        decoder["'"] = "%27";
        decoder["("] = "%28";
        decoder[")"] = "%29";
        decoder["*"] = "%2A";
        decoder["+"] = "%2B";
        decoder[","] = "%2C";
        decoder["-"] = "%2D";
        decoder["."] = "%2E";
        decoder["/"] = "%2F";
        decoder[":"] = "%3A";
        decoder[";"] = "%3B";
        decoder["<"] = "%3C";
        decoder["="] = "%3D";
        decoder[">"] = "%3E";
        decoder["?"] = "%3F";
        decoder["@"] = "%40";
        decoder["["] = "%5B";
        decoder["\\"] = "%5C";
        decoder["]"] = "%5D";
        decoder["^"] = "%5E";
        decoder["_"] = "%5F";
        decoder["`"] = "%60";
        decoder["{"] = "%7B";
        decoder["|"] = "%7C";
        decoder["}"] = "%7D";
        decoder["~"] = "%7E";

        string res = encoded;

        foreach (var e in decoder.entries) {
            res = res.replace (e.value, e.key);
        }

        return res;
    }



    /*
    * Custom errors.
    */
    public errordomain ValseError {
        NOT_FOUND,
        SERVER_ERROR
    }

    public enum DebugLevel {
        NONE,
        BASIC,
        ERRORS,
        WARNINGS,
        DEBUG
    }
}
