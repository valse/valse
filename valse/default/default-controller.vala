using Valse;

namespace Valse.Default {

    /*
    * The default controller.
    *
    * It is used when no controller have been registered to help the developer get started.
    */
    internal class DefaultController : Controller {
        
        public DefaultController () {
            this.add_action ("index", this.index);
        }

        /*
        * A welcome message and some help.
        */
        public Response index () {
            return new Response.from_html ("""
            <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf8">
                    <title>Valses default page</title>
                </head>
                <body>
                    <h1>Congratulations! Your server works!</h1>
                    <p>You correctly setuped your server ... but it's empty for now.</p>
                    <p>To show your own page here, create a new controller and register it.
                    If you don't know how to do it, check out <a href="https://git.framasoft.org/Bat/valse/blob/master/README.md">the example code</a>.</p>
                </body>
            </html>
            <!-- " Meh... Atom -->
            """);
        }
    }
}
