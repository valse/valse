using GLib.FileUtils;
using Gee;

namespace Valse {

    /**
    * Default render engine for Valse.
    */
    public class Wala : Object, RenderEngine {

        /**
        * Name of the render engine.
        */
        public string name { get; set; default = "Wala"; }

        /*
        * All the registered Wala filters.
        */
        private static HashMap<string, WalaFilter> filters { get; set; default = new HashMap<string, WalaFilter> ();}

        /**
        * Wala constructor's. It adds the default filters.
        */
        public Wala () {

            add_filter ("upper", (ref input, args) => {
                return input.up ();
            });
            add_filter ("lower", (ref input, args) => {
                return input.down ();
            });
            add_filter ("capitalize", (ref input, args) => {
                string res = "";
                for (int i = 0; i < input.length; i++) {
            		if (input.valid_char (i)) {
                        if (i == 0) {
                            res += input.get_char (i).to_string ().up ();
                        } else {
                            res += input.get_char (i).to_string ();
                        }
            		}
            	}
                return res;
            });
            add_filter ("safe", (ref input, args) => {
                string res = input.replace ("&amp;", "&");
                res = res.replace ("&quot;", "\"");
                res = res.replace ("&#039;", "'");
                res = res.replace ("&lt;", "<");
                res = res.replace ("&gt;", ">");
                return res;
            });
            add_filter ("cut", (ref input, args) => {
                if (args.length > 1) {
                    string res = input;
                    foreach (string arg in args) {
                        res = res.replace (arg, "");
                    }
                    return res;
                } else {
                    return input.replace (args [0], "");
                }
            });
            add_filter ("default", (ref input, args) => {
                if (input.length <= 0) {
                    return args [0];
                } else {
                    return input;
                }
            });
            add_filter ("linebreaks", (ref input, args) => {
                return input.replace ("\n", "<br />");
            });
            add_filter ("pluralize", (ref input, args) => {
                if (int.parse (input) > 1) {
                    if (args.length == 0) {
                        return "s";
                    } else {
                        return args [0];
                    }
                } else {
                    return "";
                }
            });
            add_filter ("truncate", (ref input, args) => {
                string striped = input.strip ();
                int limit = int.parse (args [0]);
                if (striped.length > limit) {
                    return striped.slice (0, limit-1);
                } else {
                    return striped;
                }
            });
            add_filter ("strip", (ref input, args) => {
                return input.strip ();
            });
            add_filter ("typofr", (ref input, args) => {
                while ("!!" in input) {
                    input = input.replace ("!!", "!");
                }
                while ("??" in input) {
                    input = input.replace ("??", "?");
                }
                input = input.replace ("...", "…");
                input = input.replace ("&#039;", "'"); // else it doesn't see the '
                input = input.replace ("'", "’");
                input = input.replace ("&quot;", "\""); // else it doesn't see the "
                string res = "";
                bool first_quote = true;
                for (int i = 0; i < input.length; i++) {
            		if (input.valid_char (i)) {
                        if (input.get_char (i) == '"') {
                            if (first_quote) {
                                res += "« ";
                            } else {
                                res += " »";
                            }
                        } else {
                            res += input.get_char (i).to_string ();
                        }
            		}
            	}
                return res;
            });
            add_filter ("inc", (ref input, args) => {
                int inc = 0;
                foreach (string arg in args) {
                    inc += int.parse (arg);
                }

                if (inc == 0) {
                    inc = 1;
                }

                return (int.parse (input) + inc).to_string ();
            });
            add_filter ("dec", (ref input, args) => {
                int dec = 0;
                foreach (string arg in args) {
                    dec += int.parse (arg);
                }

                if (dec == 0) {
                    dec = 1;
                }

                return (int.parse (input) - dec).to_string ();
            });
        }

        /**
        * Renders a page.
        *
        * @param view The {@link Valse.View} to render.
        */
        public string render_page (View view) {

            try {
                string result = view.content;
                result = result.replace ("{{ render_engine }}", this.name);

                string model_dump = "";

                foreach (var model in view.models.entries) {
                    string model_keyword = model.key;
                    var mdl = model.value;

                    foreach (var mod_var in mdl.entries) {
                        // dumping the new model
                        model_dump += @"<p>$(model_keyword)$(mod_var.key) = $(mod_var.value)";
                        if (mod_var.value == null) {
                            model_dump += "(null)";
                        }
                        model_dump += "</p>";
                    }

                    Regex for_re = new Regex ("{% *for (?P<item>[\\w_.]+?)(, *(?P<index>[\\w]+))? in (?P<coll>" + model_keyword + "(\\.([[:graph:]]*?))?) *%}(?P<content>(.|\\R)+?){% *endfor *%}");
                    MatchInfo for_info;
                    bool for_go = for_re.match (result, RegexMatchFlags.NEWLINE_ANY, out for_info);
                    int for_count = 0;

                    while (for_go) {
                        string item = for_info.fetch_named ("item");
                        string coll = for_info.fetch_named ("coll");
                        string content = for_info.fetch_named ("content");
                        string? index_var = for_info.fetch_named ("index");

                        string generated = "";

                        // while we haven't see every element
                        int index = 0;
                        int coll_length = int.parse (mdl [coll.replace (model_keyword, "") + ".count"]);
                        while (index < coll_length) {
                            // for each element of the collection
                            // we insert the pattern (= content of the for loop)
                            Regex item_re = new Regex ("{{ *%s(?P<prop>[[:graph:]]*) *}}".printf (item));
                            generated += item_re.replace_eval (content, content.length, 0, 0, (info, res) => {
                                string? elem_prop = info.fetch_named ("prop");
                                res.append ("{{ " + coll + "[" + index.to_string () + "]" + elem_prop + " }}");
                                return false;
                            });

                            if (index_var != null && index_var.length != 0) {
                                // interpreting index variables
                                Regex index_re = new Regex ("{{ %s(?P<filters>.*?) }}".printf (index_var));
                                generated = index_re.replace_eval (generated, generated.length, 0, 0, (info, res) => {
                                    // we don't put index directly to make it possible to apply filters
                                    string mdl_index_var = ".__for__%d__index__%d__".printf (for_count, index);
                                    mdl[mdl_index_var] = index.to_string ();
                                    res.append ("{{ %s%s%s }}".printf (model_keyword, mdl_index_var, info.fetch_named ("filters") == null ? "" : info.fetch_named ("filters")));
                                    return false;
                                });
                            }

                            index++;
                        }

                        result = for_re.replace_eval (result, result.length, 0, 0, (info, res) => {
                            res.append (generated);
                            return true;
                        });

                        for_go = for_info.next ();
                        for_count++;
                    }

                    // Model variables
                    Regex var_re = new Regex ("{{ *" + model_keyword + "(?<prop>[\\w.\\[\\]]*)(\\|(?<filter>[[:graph:]]*?)( (?<params>\".*?\"))?)? *}}");
                    MatchInfo var_info;
                    bool go_var = var_re.match (result, 0, out var_info);

                    while (go_var) {
                        string prop_name = var_info.fetch_named ("prop");
                        string prop_val = mdl [prop_name];
                        if (prop_val == null && log_level == DebugLevel.DEBUG) {
                            prop_val = "[WARNING] %s%s is null.".printf (model_keyword, prop_name.has_prefix ("[") ? prop_name : prop_name);
                        }
                        string? var_filters = var_info.fetch_named ("filter");
                        string? params = var_info.fetch_named ("params");

                        // HTML characters escape
                        prop_val = prop_val.replace ("&", "&amp;");
                        prop_val = prop_val.replace ("\"", "&quot;");
                        prop_val = prop_val.replace ("'", "&#039;");
                        prop_val = prop_val.replace ("<", "&lt;");
                        prop_val = prop_val.replace (">", "&gt;");

                        // apply filters if needed
                        if (var_filters != null && var_filters.length > 0) {
                            foreach (string filter in var_filters.split ("|")) {
                                if (Wala.filters.has_key (filter)) {
                                    string[] filter_args = {};
                                    if (params != null && params.length > 0) {
                                        foreach (string param in params.split ("\"")) {
                                            if (param.length > 0 && param != " ") {
                                                filter_args += param.strip ();
                                            }
                                        }
                                    }
                                    prop_val = filters[filter].cb (ref prop_val, filter_args);
                                } else {
                                    log_error ("Filter %s doesn't exists.".printf (filter));
                                }
                            }
                        }

                        result = var_re.replace_eval (result, result.length, 0, 0, (info, res) => {
                            res.append (prop_val);
                            return true;
                        });
                        go_var = var_re.match (result, 0, out var_info);
                    }
                }

                // global template setup
                Regex temp_re = new Regex ("{% *extends '(?P<template>.*)' *%}");
                MatchInfo temp_info;

                if (temp_re.match (result, 0, out temp_info)) {
                    string template_path = temp_info.fetch_named ("template");
                    string template = "";
                    get_contents (template_path, out template);

                    // we get zones in the current file
                    HashMap<string, string> zones = new HashMap<string, string> ();

                    bool in_zone = false;
                    string current_zone = "";
                    string current_zone_name = "";

                    foreach (string line in result.split ("\n")) {
                        Regex end_zone_re = new Regex ("{% *endzone *%}");
                        MatchInfo end_zone_info;

                        if (end_zone_re.match (line, 0, out end_zone_info)) {
                            in_zone = false;
                            zones [current_zone_name] = current_zone;
                            current_zone = "";
                        }

                        if (in_zone) {
                            current_zone += "\n" + line;
                        }

                        Regex begin_zone_re = new Regex ("{% *zone (?P<zone>[[:graph:]]*) *%}");
                        MatchInfo begin_zone_info;

                        if (begin_zone_re.match (line, 0, out begin_zone_info)) {
                            in_zone = true;
                            current_zone_name = begin_zone_info.fetch_named ("zone");
                        }
                    }

                    // we get zones in the template
                    Regex temp_zones_re = new Regex ("{% *zone (?P<zone>[[:graph:]]*) *%}");
                    MatchInfo temp_zones_info;
                    bool go_temp_zones = temp_zones_re.match (template, 0, out temp_zones_info);

                    while (go_temp_zones) {

                        string zone_name = temp_zones_info.fetch_named ("zone");

                        template = temp_zones_re.replace_eval (template, template.length, 0, 0, (info, res) => {
                                if (zones.has_key (zone_name)) {
                                    res.append (zones [zone_name]);
                                }
                                return true;
                        });

                        go_temp_zones = temp_zones_info.next ();
                    }

                    result = template;
                }

                // Included files
                Regex include_re = new Regex ("{% *include '(?P<file>.*)' *%}");
                MatchInfo inc_info;
                bool go_inc = include_re.match (result, 0, out inc_info);

                while (go_inc) {

                    string file_name = inc_info.fetch_named ("file");
                    string file_content = "";
                    get_contents (file_name, out file_content);

                    View included = new View ();
                    included.content = file_content;
                    included.models = view.models;

                    file_content = this.render_page (included);

                    result = include_re.replace_eval (result, result.length, 0, 0, (info, res) => {
                        res.append (file_content);
                        return true;
                    });

                    go_inc = inc_info.next ();
                }

                // Tags
                Regex tag_re = new Regex ("{% *tag '(?P<cond>.*)' *%}(?P<content>.*){% *endtag *%}");
                MatchInfo tag_info;
                bool tag_go = tag_re.match (result, 0, out tag_info);

                while (tag_go) {
                    bool found = false;
                    foreach (string tag in view.tags) {
                        if (tag_info.fetch_named ("cond") == tag) {
                            result = tag_re.replace (result, result.length, 0, tag_info.fetch_named ("content"));
                            found = true;
                        }
                    }
                    if (!found) {
                        result = tag_re.replace (result, result.length, 0, "");
                    }

                    tag_go = tag_info.next ();
                }

                result = result.replace ("\n", "\r\n");

                // minification
                if (Router.options.minify) {
                    result = result.replace ("\n", "");
                    result = result.replace ("\t", "");
                }

                result = result.replace ("{{ debug:model }}", model_dump);

                return result;
            } catch (Error err) {
                return Errors.server_error ("Error while rendering page using Wala : %s".printf (err.message)).body;
            }
        }

        /**
        * Registers a new filter.
        *
        * @param name The name of the new filter.
        * @param cb The callback for this filter.
        */
        public static void add_filter (string name, owned FilterCallback cb) {
            filters [name] = new WalaFilter ((owned) cb);
        }

    }
}
