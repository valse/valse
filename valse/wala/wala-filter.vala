namespace Valse {

    /**
    * The delegate representing the callback of a filter.
    */
    public delegate string FilterCallback (ref string input, string[] args);

    /**
    * Wrapper for the {@link Valse.FilterCallback} delegate.
    */
    public class WalaFilter : Object {

        /**
        * The callback for this filter.
        */
        public FilterCallback cb { get; owned set; }

        /**
        * Creates a new filter, using the given callback.
        *
        * @param filter_cb The callback for this filter.
        */
        public WalaFilter (owned FilterCallback filter_cb) {
            this.cb = (owned) filter_cb;
        }

    }

}
