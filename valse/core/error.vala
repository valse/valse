namespace Valse {

    /**
    * Contains some methods to help showing errors
    */
    namespace Errors {

        /**
        * Shows a 404 error.
        *
        * @param path The path of the file that's not found
        * @return A {@link Valse.Response} containing a 404 error page.
        */
        public Response not_found (string path) {
            DateTime time = new DateTime.now_utc ();
            string page = """
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="uft-8">
                <title>404 error</title>
            </head>
            <body>
                <h1>404 : Not found</h1>
                <h3>Unable to find page at <em>%s</em></h3>
                <p>Server running Valse - %s
            </body>
            </html>
            """.printf (path, time.format (DEBUG_DATE_FORMAT)); // date and time printing
            Response res = new Response.from_html (page);
            res.error_code = 404;
            return res;
        }

        /**
        * Shows a 500 error.
        *
        * @param message The message of the error.
        * @return A {@link Valse.Response} containing a 500 error page.
        */
        public Response server_error (string message) {
            DateTime time = new DateTime.now_utc ();
            string page = """
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="uft-8">
                <title>500 error</title>
            </head>
            <body>
                <h1>500 : Server error</h1>
                <h3>%s</h3>
                <p>Server running Valse - %s
            </body>
            </html>
            """.printf (message, time.format (DEBUG_DATE_FORMAT)); // date and time printing
            Response res = new Response.from_html (page);
            res.error_code = 500;
            return res;
        }

    }

}
