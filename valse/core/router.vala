using Gee;
using Sqlite;
using Valse.Default;

namespace Valse {

    /**
    * Used to let the user use his own static file server.
    *
    * @param req The {@link Valse.Request} made by the user.
    * @return A {@link Valse.Response} representing the static file.
    */
    public delegate Response CustomStaticServer (Request req);

    /**
    * Base class of the server.
    *
    * It lets you register controllers, mime-types ...
    */
    public class Router : Object {

        /**
        * The port on which SCGI listen
        */
        private uint16 port { get; set; }

        /**
        * The routes and the associed controllers.
        */
        private HashMap<string, Controller> routes { get; set; default = new HashMap<string, Controller> (); }

        /**
        * The {@link Valse.RouterOptions} of this.
        */
        public static RouterOptions options { get; set; default = new RouterOptions (); }

        /**
        * The different mime-types known by the server.
        */
        public HashMap<string, string> mime_types { get; set; default = new HashMap<string, string> (); }

        /**
        * The static file server to use.
        */
        public CustomStaticServer static_server { get; owned set; }

        /**
        * The errror controller to use for controller which doesn't have one.
        */
        public ErrorController error_controller { get; set; default = new ErrorController (); }

        /**
        * A list of the static files found at the launch of the server
        */
        private string[] static_files { get; set; }

        /**
        * Constructor of the server. Registers some commmons mime-types.
        *
        * @param port The port on which to listen.
        */
        public Router (uint16 port = 9000) {
            this.port = port;
            this.register_mime ("css",  "text/css");
            this.register_mime ("html", "text/html");
            this.register_mime ("png",  "image/png");
            this.register_mime ("gif",  "image/gif");
            this.register_mime ("jpg",  "image/jpeg");
            this.register_mime ("jpeg", "image/jpeg");
            this.register_mime ("ico",  "image/x-icon");
            this.register_mime ("xml",  "application/xml");
            this.register_mime ("json", "application/json");
            this.register_mime ("js",   "application/javascript");

            this.static_files = this.get_static_files_list ();
        }

        /**
        * Gets all the file of the folder
        *
        * @param static_path The folder to look up
        * @return The path list of the folder files
        */
        public string[] get_static_files_list (string dir = "static") {
            string[] res = {};
            try {
                var lib_path = File.new_for_path (dir);
                var enumerator = lib_path.enumerate_children (FileAttribute.STANDARD_NAME, 0, null);
                var file_info = enumerator.next_file (null);

                while (file_info != null) {
                    if (file_info.get_file_type () == FileType.DIRECTORY) {
                        string subdir = string.join("/", dir, file_info.get_name ());
                        foreach (string file in get_static_files_list(subdir)) {
                            res += file;
                        }
                    } else {
                        res += string.join("/", dir, file_info.get_name ());
                    }
                    file_info = enumerator.next_file (null);
                }
            } catch (Error err) {
                log_warning ("The \"static/\" folder doesn't exist or it's empty");
            }
            return res;
        }

        /**
        * Registers a new mime-type.
        *
        * @param ext The file extension
        * @param mime The associed
        */
        public void register_mime (string ext, string mime) {
            this.mime_types [ext] = mime;
        }

        /**
        * Retrives the mime-type of for a given file
        *
        * @param path The file we want to get the mime type
        * @return The mime type of the file
        */
        public string get_mime (string path) {

            string[] splitted_path = path.split (".");
            string ext = splitted_path [splitted_path.length - 1]; // on récupère juste la dernière partie du chemin

            string result = "text/plain";

            if (this.mime_types [ext] != null) {
                result = this.mime_types [ext];
            }

            return result;
        }

        /**
        * Removes n elements at the begining of an array of string
        * @param count The number of element to remove
        * @param arr The array where we remove elements
        * @return An array without the elements
        */
        private string[] strip_array (int count, string[] arr) {
            string[] res = new string[] {};
            for (int i = 0; i < arr.length; i++) {
                if (i > count - 1) {
                    res += arr [i];
                }
            }
            return res;
        }

        /**
        * Handles every incoming request.
        *
        * @param req The {@link SCGI.Request} to handle.
        */
        private void scgi_handler (SCGI.Request req) {
            try {
                var request = new Request (req);
                var res = route (request);
                string response = "Status: %d %s\r\n".printf (res.error_code, get_reason (res.error_code));
                foreach (var hdr in res.headers.entries) {
                    response += "%s: %s\r\n".printf (hdr.key, hdr.value);
                }

                foreach (var cookie in res.cookies.entries) {
                    response += "Set-Cookie: %s=%s\r\n".printf (cookie.key, cookie.value);
                }

                response += "\r\n";
                if (!res.is_data) {
                    response += res.body.replace ("\n", "\r\n");
                    req.output.write (response.data);
                } else {
                    req.output.write (response.data);
                    req.output.write (res.raw_data);
                }
            } catch (Error err) {
                log_error (err.message);
            }
        }

        private Response route (Request req) {
            Response res = new Response.from_html ("<h1>You found a bug in Valse</h1>");
            res.error_code = 500;
            string debug_message = "";

            DateTime now = new DateTime.now_local ();
            debug_message = "[%s] Request on \"%s\" -> ".printf (now.format (DEBUG_DATE_FORMAT), req.path);

            string[] clean_path = split_path (req.path);
            bool is_static = false;

            if (clean_path.length > 0) {
                if (this.static_server != null) {
                    res = this.static_server (req);
                } else {
                    // use the default static server
                    if ("static" + req.path in this.static_files) {
                        try {
                            uint8[] file_data = "".data;
                            GLib.FileUtils.get_data ("static" + req.path, out file_data);
                            res.raw_data = file_data;
                            res.is_data = true;
                            res.error_code = 200;

                            is_static = true;
                            res.mime_type = get_mime (req.path);
                            const string http_date_format = "%a, %d %b %Y %T GMT";
                            res.headers["Expires"] = new DateTime.now_utc ().add_years (1).format (http_date_format);
                            res.headers["Last-Modified"] = new DateTime.now_utc ().add_seconds (-10.0).format (http_date_format);
                        } catch (Error err) {
                            log_error ("\"static%s\" not found.".printf (req.path));
                            res.error_code = 404;
                        }
                    }
                }
            }

            if (!is_static) {
                if (this.routes.size == 0) {
                    // no routes have been registered
                    string html = """
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <meta charset="utf8">
                            <title>Valses default page</title>
                        </head>
                        <body>
                            <h1>Congratulations! Your server works!</h1>
                            <p>You correctly setuped your server ... but it's empty for now.</p>
                            <p>To show your own page here, create a new controller and register it.
                            If you don't know how to do it, check out <a href="%s">the example code</a>.</p>
                        </body>
                    </html>
                    """.printf ("https://git.framasoft.org/Bat/valse/blob/master/README.md"); // i do that, else atom's coloration bugs

                    res.body = html;
                } else {
                    Controller ctrl = new DefaultController ();
                    string act = "index";

                    if (this.routes.has_key (clean_path[0])) {
                        ctrl = this.routes[clean_path[0]];
                        clean_path = clean_path[1:clean_path.length];
                    } else if (this.routes.has_key ("home")) {
                        ctrl = this.routes["home"];
                    } else {
                        return ctrl.run_action ("index", req);
                    }

                    if (ctrl.actions.has_key (clean_path[0])) {
                        act = clean_path[0];
                        clean_path = clean_path[1:clean_path.length];
                    } else if (ctrl.actions.has_key ("index")) {
                        act = "index";
                    } else {
                        return ctrl.error_controller.error (404, "Can't find the '%s' action.".printf (clean_path [0]));
                    }

                    req.options = clean_path;
                    res = ctrl.run_action (act, req);

                    debug_message += "%s %s %d\n".printf (req.method, res.mime_type, res.error_code);
                }
            }
            debug_message += "%s %s %u\n".printf (req.method, res.mime_type, res.error_code);
            log_debug (debug_message);

            return res;
        }

        /**
        * Registers a new controller.
        *
        * @param route The route of the controller
        * @param controller The controller associed with this route.
        */
        public void register (string route, Controller controller) {
            if (!this.routes.has_key (route)) {
                if (controller.error_controller == null) {
                    controller.error_controller = this.error_controller;
                }
                this.routes[route] = controller;
            } else {
                log_error ("\"/%s\" has already a controller.".printf (route));
            }
        }

        /**
        * Run the server.
        */
        public void listen () {
            DateTime now = new DateTime.now_local ();
            log (now.format (DEBUG_DATE_FORMAT));
            log ("%s version %s".printf (NAME, VERSION));
            log ("Development server available at http://127.0.0.1");
            log ("SimpleCGI listening on port %u".printf (this.port));
            log ("%d static files found".printf (this.static_files.length));
            foreach (string file in this.static_files) {
                log_debug ("    - %s".printf (file));
            }
            log ("Quit the server with CTRL-C");
            MainLoop ml = new MainLoop ();
            new SCGI.Server (this.port, options.process_count, scgi_handler);
            ml.run ();
        }
    }

}
