using Gee;

namespace Valse {

    /**
    * Describes a view, used by the renders engines.
    */
    public class View : Object {

        /**
        * The tags of the view.
        */
        public string[] tags { get; set; }

        /**
        * The content of the view.
        */
        public string content { get; set; }

        /**
        * The modèle, a {@link Gee.Map}.
        */
        public HashMap<string, HashMap<string, string>> models { get; set; }

        /**
        * Create a new view, using the minimal HTML as content and an empty model.
        */
        public View () {
            this.content = """
            <html>
                <head>
                </head>
                <body>
                </body>
            </html>
            """;

            this.models = new HashMap<string, HashMap<string, string>> ();
        }
    }
}
