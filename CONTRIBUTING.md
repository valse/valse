# Contribution guide

## Compile Valse

### Linux (Fedora is recommended)

```
git clone https://framagit.org/valse/valse.git
make
```

You will need the following package (on Fedora) to make it works.

- vala-devel
- vala
- make
- gcc
- libsoup-devel
- libgee-devel
- sqlite-devel

You'll also need [libscgi-1.0](https://framagit.org/valse/libscgi).

### Windows

Run this script in Powershell.

```
git clone https://framagit.org/valse/valse.git
./build
```

But you must install thoose programs first:

- [git](https://git-scm.com/download/win)
- [vala](http://www.tarnyko.net/dl/vala)
- [ValaWinPKG](http://www.tarnyko.net/dl/valawinpkg) to install libgee, sqlite3 and libsoup.

## Running tests

To run tests, you must in order start nginx and make it listening for SCGI on the 9000 port.
You can find a nginx.config file to do that [here](https://framagit.org/valse/valse/snippets/282).

Then, on Linux do `make test` and go on `http://localhost/` to test what you want.

On windows, tests would start by themselve after the compilation, so make sure to run nginx before compiling.

You can also compile and run unit tests with `make unit`.

## Styleguide

We try to follow the [elementary's rules](https://elementary.io/docs/code/reference#code-style). You can check some of them automacally,
by running `ruby style.rb`.
