
# Add .exe if you are on Windows
TESTOUTPUT = tests/serv
LINUXLIB = --pkg libscgi-1.0 -X "-I." -X libscgi-1.0.so --pkg gio-2.0 --pkg gee-0.8 --pkg sqlite3
WINDOWSLIB = --pkg libscgi-1.0 -X "-I." -X libscgi-1.0.dll --pkg gio-2.0 --pkg gee-1.0 --pkg sqlite3
FILES = valse/*.vala valse/core/*.vala valse/wala/*.vala valse/default/*.vala
WINOUT = out/valse.dll
LINUXOUT = out/valse.o
LIBOUT = --library=out/valse -H out/valse.h -X -fPIC -X -shared

# TODO : use /usr/lib on 32bits systems
LIBDIR = /usr/lib64/
HEADERDIR = /usr/include/
VAPIDIR = /usr/share/vala/vapi/

linux:
	valac --vapidir=. $(LINUXLIB) $(FILES) $(LIBOUT) -o $(LINUXOUT) -X -w

windows:
	valac --vapidir=. $(WINDOWSLIB) $(FILES) $(LIBOUT) -o $(WINOUT) -X -w

.ONESHELL:
tests: linux
	cp ./out/valse.o ./tests
	cp ./out/valse.h ./tests
	cp ./out/valse.vapi ./tests
	cp libscgi-1.0.so ./tests
	cp libscgi-1.0.vapi ./tests
	cp libscgi-1.0.h ./tests
	cd ./tests
	export LD_LIBRARY_PATH=.
	valac *.vala models/*.vala --vapidir=. --pkg valse --pkg libscgi-1.0 --pkg gio-2.0 --pkg gee-0.8 -X "-I." -X "valse.o" -X "-I." -X libscgi-1.0.so -o serv
	./serv

.ONESHELL:
unit: linux
	cp ./out/valse.o ./unit-tests
	cp ./out/valse.h ./unit-tests
	cp ./out/valse.vapi ./unit-tests
	cp libscgi-1.0.so ./unit-tests
	cp libscgi-1.0.vapi ./unit-tests
	cp libscgi-1.0.h ./unit-tests
	cd ./unit-tests
	export LD_LIBRARY_PATH=.
	valac *.vala --vapidir=. --pkg valse --pkg libscgi-1.0 --pkg gio-2.0 --pkg gee-0.8 -X "-I." -X "valse.o" -X "-I." -X libscgi-1.0.so -o tests
	@echo
	@echo
	@echo "--- Starting tests ---"
	@echo
	./tests
	@echo
	@echo

install: linux
	cp ./out/valse.o $(LIBDIR)
	cp ./out/valse.h $(HEADERDIR)
	cp ./out/valse.vapi valse.deps $(VAPIDIR)

testwin: windows
	# TODO

clean:
	rm $(TESTOUTPUT)
